[CmdletBinding()]
param()

$global:Sessions.Values.ForEach({ $_ | Remove-PSSession })
$PSCmdlet.WriteVerbose("$($global:Sessions.Count) session(s) dropped")
$global:Sessions.Clear()
