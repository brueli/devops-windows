[CmdletBinding()]
param(
    [string] $SetupZip = '.\setup.zip',
    [string] $TargetPath = 'C:\bruelisoft\remote-setup'
)


# create target path on all sessions
Invoke-Command -Session @( $Sessions.Values ) -ScriptBlock {
    param($TargetPath)
    if (!(Test-Path -Path $TargetPath -PathType Container)) {
        mkdir $TargetPath
    }
} -ArgumentList $TargetPath


# copy setup.zip to all sessions
foreach ($target in $Sessions.Values) {
    Copy-Item -Path $SetupZip -Destination $TargetPath\$SetupZip -ToSession $target
}


# unpack setup.zip on all sessions
Invoke-Command -Session @( $Sessions.Values ) -ScriptBlock {
    param($TargetPath,$SetupZip)
    Expand-Archive -Path $TargetPath\$SetupZip -DestinationPath $TargetPath -Force
} -ArgumentList $TargetPath,$SetupZip
