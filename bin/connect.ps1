[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [Hashtable] $WinRmDefaults = @{
        ComputerName = 'localhost'
        Port = 5986
        UseSSL = $true
        Credential = @{
            UserName = 'vagrant'
            PasswordText = $env:USERPASSWORD
            PasswordHint = 'Enter password for user "vagrant"'
        }
        SessionOption = @{
            SkipCACheck = $true
            SkipCNCheck = $true
            SkipRevocationCheck = $true
        }
    },
    [Hashtable] $WinRmHosts = @{
        'mycomputer' = @{ 
            Port=10002 
        }
    },
    [Hashtable] $SshHosts = @{
        'mycomputer' = @{ 
            HostName = 'localhost'
            Port = 10003
        }
    },
    [Hashtable] $SshCredential = @{
        UserName = 'vagrant'
        SshKeyFilePath = ''
    }
)

Import-Module $PSScriptRoot\..\PSModules\PSParameters\PSParameters.psm1 -Verbose -Force
Import-Module $PSScriptRoot\..\PSModules\PSConnect\PSConnect.psm1 -Verbose -Force

# create new session list
$newSessions = @{}

# process WinRM hosts
if ($WinRmHosts) {
    
    # build WinRM session arguments
    $winrm_hostlist = Get-WinRmHosts $WinRmHosts $WinRmDefaults

    # connect WinRM sessions
    foreach ($hostname in $winrm_hostlist.Keys) {
        if ($PSCmdlet.ShouldProcess($hostname, 'Connecting')) {
            $hostOpts = $winrm_hostlist[$hostname].ToArgs() 
            $PSCmdlet.WriteVerbose("Connecting $($hostname)...")
            $newSessions[$hostname] = New-PSSession @hostOpts
        }
    }
}

# process SSH hosts
#if ($SshHosts) {
#    foreach ($hostname in $SshHosts.Keys) {
#        $hostOpts = $SshHosts[$hostname]
#        $hostOpts['SSHTransport'] = $true
#        $hostOpts['UserName'] = Select-Value $SshHosts[$hostname].UserName $SshCredential.UserName
#        $hostOpts['KeyFilePath'] = Select-Value $SshHosts[$hostname].KeyFilePath $SshCredential.KeyFilePath
#        $newSessions[$hostname] = New-PSSession @hostOpts 
#    }
#)

# assign new sessions
$global:Sessions = $newSessions
