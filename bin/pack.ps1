[CmdletBinding()]
param(
    [string] $WorkingFolder = '..',
    [string[]] $Path = @(
        'PSModules/*'
        'Bootstrap/bin'
        'Bootstrap/Environment'
    ),
    [string] $Destination = '.\setup.zip'
)

# load [ZipArchive] classes
$compression_assembly = [System.Reflection.Assembly]::LoadWithPartialName("System.IO.Compression")
if (!$compression_assembly) {
    throw "Cannot load assembly: [System.IO.Compression]"
}

# set working folder for .NET Framework classes
[System.Environment]::CurrentDirectory = (Get-Location).Path

# remote the .zip file if it exists
if (Test-Path -Path $Destination -PathType Leaf) {
    Remove-Item $Destination
}

# create a new .zip file
$zip_filestream = [System.IO.File]::Create($Destination, 64kB, [System.IO.FileOptions]::RandomAccess)
$zip_archive = [System.IO.Compression.ZipArchive]::new($zip_filestream, [System.IO.Compression.ZipArchiveMode]::Create)

Push-Location $WorkingFolder
try {

    $thisRoot = (Get-Location).Path

    foreach ($path_entry in $Path) {

        # write a warning if a source folder does not exist
        if (!(Test-Path -Path $path_entry)) {
            $PSCmdlet.WriteWarning("Path not found: $path_entry")
            continue
        }

        # resolve wildcards in path entries
        foreach ($thisPath in (Resolve-Path $path_entry | ForEach-Object { $_.Path })) {

            # enum files in source folder
            foreach ($thisChild in (Get-ChildItem $thisPath -Recurse)) {
                
                # open source file for reading
                $sourcefile_filestream = [System.IO.File]::OpenRead($thisChild.FullName)
                try {
                    # get entry name for current file
                    $entry_name = $thisChild.FullName.Replace("$thisRoot\", [string]::Empty).Replace("\","/")
                    $PSCmdlet.WriteVerbose("Adding $entry_name")
                    
                    # create zip entry for current file
                    $zip_entry = $zip_archive.CreateEntry($entry_name)

                    # copy source file into zip entry
                    $zip_entrystream = $zip_entry.Open()
                    try {
                        $sourcefile_filestream.CopyTo($zip_entrystream)
                        #$zip_entrystream.SetLength($sourcefile_filestream.Length)
                    }
                    finally {
                        $zip_entrystream.Dispose()
                    }

                    # add entry to .zip file
                    # $zip_archive.Entries.Add($zip_entry)

                } finally {
                    # close source file
                    $sourcefile_filestream.Dispose()
                }
                
            }

        }

    }

}
finally {
    # revert working folder
    Pop-Location

    # close the .zip file
    $zip_archive.Dispose()
    $zip_filestream.Dispose()
}
