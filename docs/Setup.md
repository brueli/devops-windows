Setup
=====


Development Environment with Vagrant
------------------------------------

```powershell
choco install -y virtualbox vagrant

cd vagrant/empty

vagrant up
```


Use in your project
-------------------

```powershell
choco install -y virtualbox vagrant

Set-Location C:/Projects/
mkdir C:/Projects/myproject
git init

git subtree add --prefix devops git@git... develop --squash
```

Update in your project
```powershell
Set-Location C:/Projects/myproject

git subtree pull --prefix devops git@git... develop --squash
```

