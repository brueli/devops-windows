[Back to README](../README.md)

Certificate Creation Toolkit
============================

The `Certificates/` folder contains the *Certificate Creation Toolkit*.

It allows you to define and generate self-signed certificate chains from a configuration file.
