[Back to README](../README.md)

Connect, Pack and Deploy Scripts
================================

The term *Connect, Pack and Deploy Scripts* refers to a set of provided scripts:

* `connect.ps1`, establishes WinRM or SSH connections to a set of hosts
* `pack.ps1`, create a `setup.zip` file from a set of relative paths
* `deploy.ps1`, push a `setup.zip` file to a set of hosts and unpack it on the target hosts
* `disconnect.ps1`, close all connections established by `connect.ps1`

Use the [Shim Generator](Shim-Generator.md) to create shims for *Connect, Pack and Deploy Scripts* in different contexts.

```
Set-Location devops/Bootstrap/Environments/Sample
New-PSShimFile -Command ../../../bin/Connect.ps1 -Destination ./Connect.ps1
New-PSShimFile -Command ../../../bin/Connect.ps1 -Destination ./Pack.ps1
New-PSShimFile -Command ../../../bin/Connect.ps1 -Destination ./Deploy.ps1
New-PSShimFile -Command ../../../bin/Connect.ps1 -Destination ./Disconnect.ps1
```

For details see [Reference](Connect-Pack-And-Deploy/Reference.md)
