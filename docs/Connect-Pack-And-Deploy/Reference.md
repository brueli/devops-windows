[Back to Connect, Pack and Deploy Script](../Connect-Pack-And-Deploy.md)

Reference
=========


Connect.ps1
-----------

Connect to a list of remote hosts.

### Setup

```powershell
Set-Location devops/Boostrap/Environment/Sample
New-PSShimFile -Command ../../../bin/connect.ps1 -Destination ./connect.ps1
```

### Example Shim

```powershell
<##
    Auto-generated shim for '../../../bin/connect.ps1'
#>
[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [Hashtable] $WinRmDefaults = @{
        ComputerName = 'localhost'
        Port = 5986
        UseSSL = $true
        Credential = @{
            UserName = 'vagrant'
            PasswordText = 'vagrant'
            PasswordHint = 'Enter password for user "vagrant"'
        }
        SessionOption = @{
            SkipCACheck = $true
            SkipCNCheck = $true
            SkipRevocationCheck = $true
        }
    },
    [Hashtable] $WinRmHosts = @{
        'mycomputer' = @{ 
            Port=10002 
        }
    }
)
$invocationArgs = @{
    'WinRmDefaults' = $WinRmDefaults
    'WinRmHosts' = $WinRmHosts
}
& "../../../bin/connect.ps1" @invocationArgs
```


Pack.ps1
--------

Pack local files into a setup.zip file.

### Setup

```powershell
Set-Location devops/Boostrap/Environment/Sample
New-PSShimFile -Command ../../../bin/pack.ps1 -Destination ./pack.ps1
```

### Sample Shim

```powershell
<##
    Auto-generated shim for '../../../bin/pack.ps1'
#>
[CmdletBinding()]
param(
    [string] $WorkingFolder = '../../..',
    [string[]] $Path = @(
        'PSModules/*'
        'Bootstrap/bin'
        'Bootstrap/Environment'
    ),
    [string] $Destination = '.\setup.zip'
)
$invocationArgs = @{
    'WorkingFolder' = $WorkingFolder
    'Path' = $Path
    'Destination' = $Destination
}
& "../../../bin/pack.ps1" @invocationArgs
```


Deploy.ps1
----------

Transfer setup.zip file to remote hosts and unpack it.

### Setup

```powershell
Set-Location devops/Boostrap/Environment/Sample
New-PSShimFile -Command ../../../bin/deploy.ps1 -Destination ./deploy.ps1
```

### Example shim

```powershell
<##
    Auto-generated shim for '../../../bin/deploy.ps1'
#>
[CmdletBinding()]
param(
    [string] $SetupZip = '.\setup.zip',
    [string] $TargetPath = 'C:\bruelisoft\remote-setup'
)
$invocationArgs = @{
    'SetupZip' = $SetupZip
    'TargetPath' = $TargetPath
}
& "../../../bin/deploy.ps1" @invocationArgs
```


Disconnect.ps1
--------------

Disconnect from all remote hosts.

### Setup

```powershell
Set-Location devops/Boostrap/Environment/Sample
New-PSShimFile -Command ../../../bin/disconnect.ps1 -Destination ./disconnect.ps1
```

### Example Shim

```powershell
<##
    Auto-generated shim for '../../../bin/disconnect.ps1'
#>
[CmdletBinding()]
param()
$invocationArgs = @{
}
& "../../../bin/disconnect.ps1" @invocationArgs
```
