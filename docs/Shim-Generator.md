[Back to README](../README.md)

Shim Generator
==============

The Shim Generator (`PSModules/ShimGen`) is a powershell module to
generate shim scripts to simplify invocation of customized scripts in different folders.

Example
-------

You got a script in `C:\mybin\mytool\myscript.ps1`.

And you want to use that script from different locations, but with different parameters. This might be because of compatibility reasons or whatever.

You could copy the script over and over, but this is hell for maintenance.
Shims could be a solution for this...

```powershell
# create a new shim script
Set-Location $workingFolder
New-PSShimFile -Command 'C:\mybin\mytool\myscript.ps1' -Destination .\myscript

# this will create a compatible shim script in the working directory. 
# default parameters can be set in .\myscript.ps1

# now you can easily call myscript.ps1 from different working folders with different default parameters, depending on the location.
.\myscript.ps1
 
```