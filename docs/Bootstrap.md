[Back to README](../README.md)

Bootstrap
=========

The `Bootstrap/` folder contains the *Bootstrap Toolkit*.

*Bootstrap* is intended to configure basic services on a Windows Server to make it remotely accessible for automation tools.
