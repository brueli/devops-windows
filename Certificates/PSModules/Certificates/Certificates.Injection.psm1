
$script:CertificateFactoryMethods = @{}

function Register-CertificateType {
    [CmdletBinding()]
    [OutputType([void])]
    param(
        [Parameter(Mandatory=$true)]
        [CertificateType] $Type,
        [Parameter(Mandatory=$true)]
        [string] $Command 
    )
    
    $script:CertificateFactoryMethods[$Type] = "function:\$Command"
}

function Get-CertificateFactory {
    [CmdletBinding()]
    [OutputType([string])]
    param(
        [Parameter(Mandatory=$true)]
        [CertificateType] $Type
    )
    return $script:CertificateFactoryMethods[$Type]
}

Export-ModuleMember -Function Register-CertificateType, Get-CertificateFactory