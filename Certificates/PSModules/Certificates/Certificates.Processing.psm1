using module .\Certificates.Classes.psm1
using namespace .\Certificates.Classes.psm1

Import-Module $PSScriptRoot\Certificates.Config.psm1

Import-Module $PSScriptRoot\Certificates.Injection.psm1

Import-Module $PSScriptRoot\Certificates.CertTypes.psm1

function Get-CertificateCommand {
    param(
        [CertificateItemConfig] $Item
    )
    $factoryMethod = Get-CertificateFactory $Item.Type
    if ($factoryMethod) {
        return (Get-Item $factoryMethod).ScriptBlock
    }
    throw "CertificateType $($Item.Type) is not supported"
}

function Invoke-CertificateCreation {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [CertificateConfiguration] $Config
    )

    # ensure output folders
    if (!(Test-Path $Config.PfxFolder)) {
        mkdir -Path $Config.PfxFolder
    }
    if (!(Test-Path $Config.CerFolder)) {
        mkdir -Path $Config.CerFolder
    }
    if (!(Test-Path $Config.CsrFolder)) {
        mkdir -Path $Config.CsrFolder
    }

    # evaluate date expressions
    foreach ($cert in $Config.Certificates) {
        $cert.NotBefore = $Config.GetDateValue($cert.NotBeforeExpr)
        $cert.NotAfter = $Config.GetDateValue($cert.NotAfterExpr)
    }

    # ensure root certificate
    foreach ($cert in $Config.Certificates.Where({ 
        $_.Type -eq [CertificateType]::Root
    })) {
        $factoryMethod = Get-CertificateCommand $cert
        $rootCert = $factoryMethod.InvokeReturnAsIs($Config, $cert)
    }

    # ensure other certificates
    foreach ($cert in $Config.Certificates.Where({
        $_.Type -ne [CertificateType]::Root
    })) {
        $factoryMethod = Get-CertificateCommand $cert
        $otherCert = $factoryMethod.InvokeReturnAsIs($Config, $cert)
    }

    # read the export password
    $exportPassword = Read-Host -Prompt "Enter Password for PFX Export" -AsSecureString
    
    # export generated certificate requests to CSR folder

    # export generated certificates to CER folder
    # export generated private keys to PFX folder
    foreach ($cert in $Config.Output.Certificates) {
        $certConfig = $Config.Certificates.Where({ $_.FriendlyName -eq $cert.FriendlyName })
        $cerFileName = "{0}.cer" -f $certConfig.Id
        $pfxFileName = "{0}.pfx" -f $certConfig.Id
        $cerExportPath = Join-Path $Config.CerFolder $cerFileName
        $pfxExportPath = Join-Path $Config.PfxFolder $pfxFileName
        Export-Certificate -Cert $cert -FilePath $cerExportPath
        Export-PfxCertificate -Cert $cert -FilePath $pfxExportPath -Password $exportPassword
    }

}

Export-ModuleMember -Function Invoke-CertificateCreation, Get-CertificateConfiguration
