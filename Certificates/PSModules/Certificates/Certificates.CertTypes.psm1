using module .\Certificates.Classes.psm1
using namespace .\Certificates.Classes.psm1

Import-Module $PSScriptRoot\Certificates.Injection.psm1

$OID_ExtendedKeyUsage = '2.5.29.37'

$OID_ServerAuth = '1.3.6.1.5.5.7.3.1'
$OID_ClientAuth = '1.3.6.1.5.5.7.3.2'
$OID_CodeSigning = '1.3.6.1.5.5.7.3.3'
$OID_EmailProtection = '1.3.6.1.5.5.7.3.4'
$OID_TimeStamping = '1.3.6.1.5.5.7.3.8'
$OID_OCSPSigning = '1.3.6.1.5.5.7.3.9'
$OID_DocumentEncryption = '1.3.6.1.4.1.311.80.1'


function Compare-Certificate {
    [OutputType([bool])]
    param(
        [System.Security.Cryptography.X509Certificates.X509Certificate2] $Certificate,
        [CertificateItemConfig] $CertificateConfig
    )
    return $CertificateConfig.FriendlyName -eq $Certificate.FriendlyName
}


function New-RootCertificate {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [CertificateConfiguration] $Config,
        [Parameter(Mandatory=$true)]
        [CertificateItemConfig] $Certificate
    )

    $rootCertOpts = @{
        Type = 'Custom'
        CertStoreLocation = $Config.TargetStore
        KeyUsage = 'CertSign','CRLSign','DigitalSignature'
        KeyLength = 3072
        KeyAlgorithm = 'RSA'
        Subject = $Certificate.Subject
        FriendlyName = $Certificate.FriendlyName
        NotBefore = $Certificate.NotBefore
        NotAfter = $Certificate.NotAfter
    }

    $existingCert = (Get-ChildItem $Config.TargetStore).Where({
        Compare-Certificate -Certificate $_ -CertificateConfig $Certificate
    }) | Select-Object -First 1

    if (!$existingCert) {
        $newCert = New-SelfSignedCertificate @rootCertOpts
        $Config.Output.Register($Certificate, $newCert)
    } else {
        $Config.Output.Register($Certificate, $existingCert)
    }
}


function New-TlsServerCertificate {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [CertificateConfiguration] $Config,
        [Parameter(Mandatory=$true)]
        [CertificateItemConfig] $Certificate
    )

    $tlsServerCertOpts = @{
        Type = 'SSLServerAuthentication'
        CertStoreLocation = $Config.TargetStore
        KeyUsage = 'DigitalSignature','KeyEncipherment','KeyAgreement'
        KeyLength = 3072
        KeyAlgorithm = 'RSA'
        Subject = $Certificate.Subject
        FriendlyName = $Certificate.FriendlyName
        NotBefore = $Certificate.NotBefore
        NotAfter = $Certificate.NotAfter
        TextExtension = "${OID_ExtendedKeyUsage}={text}${OID_ServerAuth}"
        Signer = $Config.ResolveCertificate($Certificate.Signer)
    }

    $existingCert = (Get-ChildItem $Config.TargetStore).Where({
        Compare-Certificate -Certificate $_ -CertificateConfig $Certificate
    }) | Select-Object -First 1

    if (!$existingCert) {
        $newCert = New-SelfSignedCertificate @tlsServerCertOpts
        $Config.Output.Certificates.Add($newCert)
    } else {
        $Config.Output.Certificates.Add($existingCert)
    }    
}


function New-TlsClientCertificate {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [CertificateConfiguration] $Config,
        [Parameter(Mandatory=$true)]
        [CertificateItemConfig] $Certificate
    )

    $tlsClientCertOpts = @{
        Type = 'Custom'
        CertStoreLocation = $Config.TargetStore
        KeyUsage = 'DigitalSignature','KeyAgreement'
        KeyLength = 3072
        KeyAlgorithm = 'RSA'
        Subject = $Certificate.Subject
        FriendlyName = $Certificate.FriendlyName
        NotBefore = $Certificate.NotBefore
        NotAfter = $Certificate.NotAfter
        TextExtension = "${OID_ExtendedKeyUsage}={text}${OID_ClientAuth}"
        Signer = $Config.ResolveCertificate($Certificate.Signer)
    }

    $existingCert = (Get-ChildItem $Config.TargetStore).Where({
        Compare-Certificate -Certificate $_ -CertificateConfig $Certificate
    }) | Select-Object -First 1

    if (!$existingCert) {
        $newCert = New-SelfSignedCertificate @tlsClientCertOpts
        $Config.Output.Certificates.Add($newCert)
    } else {
        $Config.Output.Certificates.Add($existingCert)
    }    
}


function New-DscEncryptionCertificate {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [CertificateConfiguration] $Config,
        [Parameter(Mandatory=$true)]
        [CertificateItemConfig] $Certificate
    )

    $dscCertOpts = @{
        Type = 'Custom'
        CertStoreLocation = $Config.TargetStore
        KeyUsage = 'KeyEncipherment','DataEncipherment'
        KeyLength = 3072
        KeyAlgorithm = 'RSA'
        Subject = $Certificate.Subject
        FriendlyName = $Certificate.FriendlyName
        NotBefore = $Certificate.NotBefore
        NotAfter = $Certificate.NotAfter
        TextExtension = "${OID_ExtendedKeyUsage}={text}${OID_DocumentEncryption}"
        Signer = $Config.ResolveCertificate($Certificate.Signer)
    }

    $existingCert = (Get-ChildItem $Config.TargetStore).Where({
        Compare-Certificate -Certificate $_ -CertificateConfig $Certificate
    }) | Select-Object -First 1

    if (!$existingCert) {
        $newCert = New-SelfSignedCertificate @dscCertOpts
        $Config.Output.Certificates.Add($newCert)
    } else {
        $Config.Output.Certificates.Add($existingCert)
    }    
}


Register-CertificateType -Type Root -Command New-RootCertificate
Register-CertificateType -Type TlsServer -Command New-TlsServerCertificate
Register-CertificateType -Type TlsClient -Command New-TlsClientCertificate
Register-CertificateType -Type PowershellDsc -Command New-DscEncryptionCertificate


