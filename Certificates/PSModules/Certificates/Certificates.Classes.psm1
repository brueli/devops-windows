using module ..\..\..\PSModules\PSConfigFile\PSConfigFile.psm1
using namespace ..\..\..\PSModules\PSConfigFile\PSConfigFile.psm1


enum CertificateType {
    Root
    WinRmHttps
    PowershellDsc
    TlsServer
    TlsClient
}


class CertificateItemConfig : ConfigSection {
    [CertificateType] $Type
    [string] $Id
    [string] $Name
    [string] $Subject
    [string] $FriendlyName
    [string] $NotBeforeExpr
    [string] $NotAfterExpr
    [string] $Signer
    [string[]] $DnsNames

    [DateTime] $NotBefore
    [DateTime] $NotAfter

    CertificateItemConfig([CertificateConfiguration] $root, [Hashtable] $config, [string] $context)
        : base($root, $config, $context) 
    {
        $this.Type = [CertificateType] [Enum]::Parse([CertificateType], $this.Require($config.Type, "$context.Type"), $true)
        $this.Id = $this.Require($config.Id, "$context.Id")
        $this.Name = $this.SelectValue($config.Name, $this.Id)
        if (!$config.Subject) {
            $this.Subject = $this.ReplaceNameTokens($root.DefaultSubject)
        } else {
            $this.Subject = $config.Subject
        }
        if (!$config.FriendlyName) {
            $this.FriendlyName = $this.ReplaceNameTokens($root.DefaultFriendlyName)
        } else {
            $this.FriendlyName = $config.FriendlyName
        }
        $this.NotBeforeExpr = $this.SelectValue($config.NotBefore, $this.Root.DefaultNotBeforeExpr)
        $this.NotAfterExpr = $this.SelectValue($config.NotAfter, $this.Root.DefaultNotAfterExpr)
        if ($this.Type -ine 'root') {
            $this.Signer = $this.Require($config.Signer, "$context.Signer")
        }
        if ($config.Type -ieq 'tlsserver') {
            $this.DnsNames = $this.Require($config.DnsNames, "$context.DnsNames[]")
        }
    }

    [string] ReplaceNameTokens([string] $value) {
        return $value `
            -replace '{Id}',$this.Id `
            -replace '{Name}',$this.Name `
            -replace '{Version}',$this.Root.Version
    }
}


class CertificateOutputs {
    [string] $DefaultStore
    [System.Collections.Generic.List[System.Security.Cryptography.X509Certificates.X509Certificate2]] $Certificates
    hidden [System.Collections.Generic.Dictionary[string, System.Security.Cryptography.X509Certificates.X509Certificate2]] $_ResolverCache

    CertificateOutputs() {
        $this.DefaultStore = 'Cert:\CurrentUser\My'
        $this.Certificates = [System.Collections.Generic.List[System.Security.Cryptography.X509Certificates.X509Certificate2]]::new()
        $this._ResolverCache = [System.Collections.Generic.Dictionary[string, System.Security.Cryptography.X509Certificates.X509Certificate2]]::new()
    }

    [void] Register([CertificateItemConfig] $certificateItem, [System.Security.Cryptography.X509Certificates.X509Certificate2] $resolvedCertificate) {
        $certId = "@{0}" -f $certificateItem.Id
        $this._ResolverCache[$certId] = $resolvedCertificate
        $this.Certificates.Add($resolvedCertificate)
    }

    [System.Security.Cryptography.X509Certificates.X509Certificate2] Resolve([string] $certId) {
        if (!$this._ResolverCache.ContainsKey($certId)) {
            throw "Certificate $certId is not registered"
        }
        return $this._ResolverCache[$certId]
    }
}


class CertificateConfiguration : BaseConfiguration {
    [string] $PfxFolder
    [string] $CerFolder
    [string] $CsrFolder
    [string] $Version
    [string] $TargetStore
    [string] $DefaultSubject
    [string] $DefaultFriendlyName
    [string] $DefaultNotBeforeExpr
    [string] $DefaultNotAfterExpr

    [System.Collections.Generic.List[CertificateItemConfig]] $Certificates

    [CertificateOutputs] $Output

    CertificateConfiguration([Hashtable] $config, [ConfigurationFactory] $factory)
        : base($config, $factory) 
    {
        $this.PfxFolder = $this.SelectValue($config.PfxFolder, './PFX')
        $this.CerFolder = $this.SelectValue($config.CerFolder, './CER')
        $this.CsrFolder = $this.SelectValue($config.CsrFolder, './CSR')
        $this.Version = $this.SelectValue($config.Version, 1)
        $this.TargetStore = $this.SelectValue($config.TargetStore, 'Cert:\CurrentUser\My')
        $this.DefaultSubject = $this.Require($config.DefaultSubject, 'DefaultSubject')
        $this.DefaultFriendlyName = $this.Require($config.DefaultFriendlyName, 'DefaultFriendlyName')
        $this.DefaultNotBeforeExpr = $this.SelectValue($config.DefaultNotBefore, '[Get-Date]')
        $this.DefaultNotAfterExpr = $this.SelectValue($config.DefaultNotAfter, '[(Get-Date).AddDays(1*365)]')

        $this.Output = [CertificateOutputs]::new()

        $this.Certificates = [System.Collections.Generic.List[CertificateItemConfig]]::new()
        for ($i = 0; $i -lt $config.Certificates.Length; $i++) {
            try {
                $newItem = [CertificateItemConfig]::new($this, $config.Certificates[$i], "Certificates[$i]")
                $this.Certificates.Add($newItem)
            } catch {
                Write-Error $_
                Write-Error -Message "Error while parsing Certificate[$i]"
            }
        }
    }

    [DateTime] GetDateValue([string] $dateExpr) {
        $result = [DateTime]::MinValue
        if ($dateExpr -match '^\[.+\]$') {
            $result = [DateTime] (Invoke-Expression -Command $dateExpr.Substring(1, $dateExpr.Length-2))
        } else {
            $result = [DateTime]::Parse($dateExpr)
        }
        return $result
    }

    [System.Security.Cryptography.X509Certificates.X509Certificate2] ResolveCertificate([string] $certId) {
        $result = $null
        if ($certId.StartsWith('@')) {
            # resolve from cache
            $result = $this.Output.Resolve($certId)
        } elseif ($certId.StartsWith('Cert:\')) {
            # resolve full cert:\ path
            $result = Get-Item $certId
        } else {
            # resolve thumbprint in target store
            $signerCertPath = Join-Path $this.TargetStore $certId
            $result = Get-Item $signerCertPath
        }
        if (!$result) {
            throw "Certificate not found: $certId"
        }
        return $result
    }
}


class CertificateConfigurationBuilder : ConfigBuilder {

    CertificateConfigurationBuilder() {}

    [CertificateConfiguration] Build([Hashtable] $config, [ConfigurationFactory] $factory) {
        return [CertificateConfiguration]::new($config, $factory) 
    }
}
