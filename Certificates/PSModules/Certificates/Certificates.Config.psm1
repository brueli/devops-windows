using module ..\..\..\PSModules\PSConfigFile\PSConfigFile.psm1
using namespace ..\..\..\PSModules\PSConfigFile\PSConfigFile.psm1

using module .\Certificates.Classes.psm1
using namespace .\Certificates.Classes.psm1

function Get-CertificateConfiguration {
    [CmdletBinding()]
    [OutputType([CertificateConfiguration])]
    param(
        [Parameter(Mandatory=$true)]
        [string] $Path
    )

    # test data file path
    if (!(Test-Path -Path $Path -PathType Leaf)) {
        throw [System.ArgumentException]::new("Path must point to a valid .psd1 file", "Path")
    }

    # load data file
    $configData = Import-PowerShellDataFile -Path $Path
    if (!$?) {
        return
    }

    # create config factory
    $factory = [ConfigurationFactory]::new({ [CertificateConfigurationBuilder]::new() })
    
    # build configuration object from data
    $result = $factory.Build($configData)

    # return configuration object filled with data
    return $result
}

Export-ModuleMember -Function Get-CertificateConfiguration
