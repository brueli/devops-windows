$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = 'Certificates.Processing.psm1'
Import-Module $here\$sut -Verbose -Force

Describe("Certificates/Invoke-CertificateCreation") {
    it("Can create certificates from a valid .psd1 file") {
        
        $config = Get-CertificateConfiguration -Path $here\..\..\Environment\Sample\Certificates.config.psd1
        $config | Should Not Be $null

        Invoke-CertificateCreation -Config $config
    }
}
