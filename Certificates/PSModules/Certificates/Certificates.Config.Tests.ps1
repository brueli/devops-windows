# ./Certificates.Config.Tests.ps1
using module .\Certificates.Config.psm1
using namespace .\Certificates.Config.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = 'Certificates.Config.psm1'
Import-Module $here\$sut -Verbose -Force

Describe("Certificates/Get-CertificateConfiguration") {
    it("Can load a valid .psd1 file") {
        $config = Get-CertificateConfiguration -Path $here\..\..\Environment\Sample\Certificates.config.psd1
        $config | Should Not Be $null

        $config.PfxFolder | Should Be './PFX'
        $config.CerFolder | Should Be './CER'
        $config.CsrFolder | Should Be './CSR'
        $config.Version | Should Be '1'
        $config.TargetStore | Should Be 'Cert:\CurrentUser\My'
        $config.DefaultSubject | Should Be 'CN={Name},OU=SelfSigned,O=Bruelisoft,L=Flawil,ST=St.Gallen,C=CH'
        $config.DefaultFriendlyName | Should Be 'SelfSigned-{Name}(v{Version})'
        $config.DefaultNotBeforeExpr | Should Be '[Get-Date]'
        $config.DefaultNotAfterExpr | Should Be '[(Get-Date).AddDays(1*365)]'
    
        $config.Certificates[0].Type | Should Be 'Root'
        $config.Certificates[0].Id | Should Be 'RootCA'
        $config.Certificates[0].Subject | Should Be 'CN=RootCA,OU=SelfSigned,O=Bruelisoft,L=Flawil,ST=St.Gallen,C=CH'
        $config.Certificates[0].FriendlyName | Should Be 'SelfSigned-RootCA(v1)'
        $config.Certificates[0].NotBeforeExpr | Should Be '[Get-Date]'
        $config.Certificates[0].NotAfterExpr | Should Be '[(Get-Date).AddDays(5*365)]'

        $config.Certificates[1].Type | Should Be 'TlsServer'
        $config.Certificates[1].Id | Should Be 'WinRm'
        $config.Certificates[1].Subject | Should Be 'CN=DEMO'
        $config.Certificates[1].FriendlyName | Should Be 'SelfSigned-WinRm(v1)'
        $config.Certificates[1].NotBeforeExpr | Should Be '[Get-Date]'
        $config.Certificates[1].NotAfterExpr | Should Be '[(Get-Date).AddDays(1*365)]'
        $config.Certificates[1].Signer | Should Be '@RootCA'

        $config.Certificates[2].Type | Should Be 'PowershellDsc'
        $config.Certificates[2].Id | Should Be 'Dsc'
        $config.Certificates[2].Subject | Should Be 'CN=Dsc,OU=SelfSigned,O=Bruelisoft,L=Flawil,ST=St.Gallen,C=CH'
        $config.Certificates[2].FriendlyName | Should Be 'SelfSigned-Dsc(v1)'
        $config.Certificates[2].NotBeforeExpr | Should Be '[Get-Date]'
        $config.Certificates[2].NotAfterExpr | Should Be '[(Get-Date).AddDays(1*365)]'
        $config.Certificates[2].Signer | Should Be '@RootCA'

        $config.Certificates[3].Type | Should Be 'TlsServer'
        $config.Certificates[3].Id | Should Be 'MyWeb'
        $config.Certificates[3].Subject | Should Be 'CN=MyWeb,OU=SelfSigned,O=Bruelisoft,L=Flawil,ST=St.Gallen,C=CH'
        $config.Certificates[3].FriendlyName | Should Be 'SelfSigned-MyWeb(v1)'
        $config.Certificates[3].NotBeforeExpr | Should Be '[Get-Date]'
        $config.Certificates[3].NotAfterExpr | Should Be '[(Get-Date).AddDays(1*365)]'
        $config.Certificates[3].Signer | Should Be '@RootCA'
        $config.Certificates[3].DnsNames.Length | Should BeGreaterThan 0

        $config.Certificates[4].Type | Should Be 'TlsClient'
        $config.Certificates[4].Id | Should Be 'MyWebClient'
        $config.Certificates[4].Subject | Should Be 'CN=MyWebClient,OU=SelfSigned,O=Bruelisoft,L=Flawil,ST=St.Gallen,C=CH'
        $config.Certificates[4].FriendlyName | Should Be 'SelfSigned-MyWebClient(v1)'
        $config.Certificates[4].NotBeforeExpr | Should Be '[Get-Date]'
        $config.Certificates[4].NotAfterExpr | Should Be '[(Get-Date).AddDays(1*365)]'
        $config.Certificates[4].Signer | Should Be '@RootCA'
    }
}
