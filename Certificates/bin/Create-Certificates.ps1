[CmdletBinding()]
param(
    [string] $ConfigFile = '.\Certificates.config.psd1'
)

# resolve config file path
$resolvedPath = (Resolve-Path $ConfigFile).Path

# load config file
$config = Get-CertificateConfiguration -Path $resolvedPath

# create and export certificates
Invoke-CertificateCreation -Config $config
