@{
    PfxFolder = './PFX'
    CerFolder = './CER'
    CsrFolder = './CSR'

    Version = 1

    TargetStore = 'Cert:\CurrentUser\My'
    
    DefaultSubject = 'CN={Name},OU=SelfSigned,O=Bruelisoft,L=Flawil,ST=St.Gallen,C=CH'
    DefaultFriendlyName = 'SelfSigned-{Name}(v{Version})'
    DefaultNotBefore = '[Get-Date]'
    DefaultNotAfter = '[(Get-Date).AddDays(1*365)]'

    Certificates = @(
        @{
            Type = 'root'

            Id = 'RootCA'
            #Subject = 'CN=... full custom subject distinguished name here ... ST=St.Gallen'
            #FriendlyName = 'SelfSigned-RootCA(v1)'
            #NotBefore = [Get-Date]
            NotAfter = '[(Get-Date).AddDays(5*365)]'
        }

        @{
            Type = 'TlsServer'

            Id = 'WinRm'
            Signer = '@RootCA'
            Subject = 'CN=DEMO'
            DnsNames = @(
                'DEMO'
            )
        }

        @{
            Type = 'PowershellDsc'
            Id = 'Dsc'
            Signer = '@RootCA'
        }

        @{
            Type = 'TlsServer'
            Id = 'MyWeb'
            Signer = '@RootCA'
            DnsNames = @(
                '*.local.bruelisoft.net'
            )
        }

        @{
            Type = 'TlsClient'
            Id = 'MyWebClient'
            Signer = '@RootCA'
        }
    )
}