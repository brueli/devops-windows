﻿<##
    Auto-generated shim for '..\..\bin\Create-Certificates.ps1'
#>
[CmdletBinding()]
param(
    [string] $ConfigFile = '.\Certificates.config.psd1'
)
$invocationArgs = @{
    'ConfigFile' = $ConfigFile
}
& "..\..\bin\Create-Certificates.ps1" @invocationArgs

