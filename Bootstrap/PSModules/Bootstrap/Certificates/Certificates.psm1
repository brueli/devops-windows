using module ..\Config\classes.psm1
using namespace ..\Config\classes.psm1

Import-Module $PSScriptRoot\..\Config\Config.psm1


enum CertFileType {
    CER
    PFX
}


class CertFile : ConfigSection {
    [string] $Id
    [CertFileType] $Type
    [string] $Thumbprint
    [string] $FilePath
    [string] $FileContent
    [string] $TargetStore
    [string] $PasswordHint
    [string] $PasswordText

    CertFile([BootstrapConfiguration] $root, [Hashtable] $config, [int] $index, [string] $context)
        : base($root, $config, $context)
    {
        $this.Id = $this.Require($config.Id, "${context}.Id")
        $this.Type = [Enum]::Parse([CertFileType], $this.Require($config.Type, "${context}.Type"), $true)
        $this.Thumbprint = $this.Require($config.Thumbprint, "${context}.Thumbprint")
        $this.FilePath = $this.SelectValue($config.FilePath, $null)
        $this.FileContent = $this.SelectValue($config.FileContent, $null)
        $this.Require($this.FilePath, $this.FileContent, "${context}: FilePath or FileContent")
        $this.TargetStore = $this.Require($config.TargetStore, $root.Config.Certificates.DefaultStore, "${context}.TargetStore or Certificates.DefaultStore")
        $this.PasswordHint = $this.SelectValue($config.PasswordHint, $root.Config.Certificates.DefaultPasswordHint)
        $this.PasswordText = [string] $this.Eval($this.SelectValue($config.PasswordText, $root.Config.Certificates.DefaultPasswordText))
        $this.Require($this.PasswordHint, $this.PasswordText, "${context}.PasswordHint or ${context}.PasswordText")
    }
}


class CertificatesConfig : ConfigSection {
    [string] $DefaultPasswordText
    [string] $DefaultPasswordHint
    [string] $DefaultStore
    [System.Collections.Generic.List[CertFile]] $Files

    CertificatesConfig([BootstrapConfiguration] $root, [Hashtable] $config, [string] $context)
        : base($root, $config, $context)
    {
        $this.DefaultPasswordHint = $this.SelectValue($config.DefaultPasswordHint, $null)
        $this.DefaultPasswordText = $this.SelectValue($config.DefaultPasswordText, $null)
        $this.DefaultStore = $this.Require($config.DefaultStore, 'Cert:\LocalMachine\My')
        
        $this.Files = [System.Collections.Generic.List[CertFile]]::new()
        for ($iCert = 0; $iCert -lt $config.Files.Count; $iCert++) {
            $certFile = [CertFile]::new($root, $config.Files[$iCert], $iCert, "${context}.Files[${iCert}]")
            $this.Files.Add($certFile)
        }
    }
}


class CertificatesConfigFactory : ConfigFactory {
    CertificatesConfigFactory() : base() {}

    [ConfigSection] Build([BootstrapConfiguration] $root, [Hashtable] $config, [string] $context) {
        return [CertificatesConfig]::new($root, $config, $context)
    }
}


class CertificatesConfigProcessor : ConfigProcessor {
    CertificatesConfigProcessor() : base() {}

    [void] Invoke([BootstrapConfiguration] $configuration) {
        $certificatesConfig = [CertificatesConfig] $configuration.Sections.Certificates
        if ($certificatesConfig) {
            # install certificates
            foreach ($certFile in $certificatesConfig.Files) {
                $this.ProcessCertificate($certFile)
            }
        }
    }

    hidden [void] ProcessCertificate([CertFile] $certFile) {

        if ($certFile.Type -eq [CertFileType]::PFX) {
            if ($certFile.PasswordHint) {
                $pfxPassword = Read-Host -Prompt $certFile.PasswordHint -AsSecureString
            } else {
                $pfxPassword = ConvertTo-SecureString -String $certFile.PasswordText -AsPlainText -Force
            }
            if ($certFile.FilePath) {
                Import-PfxCertificate -FilePath $certFile.FilePath -CertStoreLocation $certFile.TargetStore -Password $pfxPassword
            } else {
                $pfxBytes = [Convert]::FromBase64String($certFile.FileContent)
                $tempFile = [System.IO.Path]::GetTempFileName()
                [System.IO.File]::WriteAllBytes($pfxBytes, $tempFile)
                Import-PfxCertificate -FilePath $tempFile -CertStoreLocation $certFile.TargetStore -Password $pfxPassword
                Remove-Item $tempFile
            }
            return
        }

        if ($certFile.FilePath) {
            Import-Certificate -FilePath $certFile.FilePath -CertStoreLocation $certFile.TargetStore
        } else {
            $cerBytes = [Convert]::FromBase64String($certFile.FileContent)
            $tempFile = [System.IO.Path]::GetTempFileName()
            [System.IO.File]::WriteAllBytes($cerBytes, $tempFile)
            Import-Certificate -FilePath $tempFile -CertStoreLocation $certFile.TargetStore
            Remove-Item $tempFile
        }
    }
}


Register-BootstrapSection -Name 'Certificates' -Factory {
    return [CertificatesConfigFactory]::new()
} -Processor {
    return [CertificatesConfigProcessor]::new()
}