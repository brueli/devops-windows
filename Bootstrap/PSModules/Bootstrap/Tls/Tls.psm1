using module ..\Config\classes.psm1
using namespace ..\Config\classes.psm1

Import-Module $PSScriptRoot\..\Config\Config.psm1


class TlsConfig : ConfigSection {
    [bool] $Present
    [bool] $Enabled

    TlsConfig([BootstrapConfiguration] $root, [Hashtable] $config, [string] $context) 
        : base($root, $config, $context) 
    {
        $this.Present = $this.Require($config.Present, "$context.Present")
        $this.Enabled = $this.SelectValue($config.Enabled, "$context.Enabled")    
    }
}


class TlsConfigFactory : ConfigFactory {
    TlsConfigFactory() : base() {}

    [ConfigSection] Build([BootstrapConfiguration] $root, [Hashtable] $config, [string] $context) {
        return [TlsConfig]::new($root, $config, $context)
    }
}


class TlsConfigProcessor : ConfigProcessor {
    TlsConfigProcessor() : base() {}

    [void] Invoke([BootstrapConfiguration] $configuration) {
        $tlsConfig = [TlsConfig] $configuration.Sections.Tls
        if ($tlsConfig.Present) {
            $dwordValue = if ($tlsConfig.Enabled) { '1' } else { '0' }
            Set-ItemProperty -Path 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\.NetFramework\v4.0.30319' -Name 'SchUseStrongCrypto' -Value $dwordValue -Type DWord             
            Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\.NetFramework\v4.0.30319' -Name 'SchUseStrongCrypto' -Value $dwordValue -Type DWord
        } else {

        }
    }
}


Register-BootstrapSection -Name 'Tls' -Factory {
    return [TlsConfigFactory]::new()
} -Processor {
    return [TlsConfigProcessor]::new()
}
