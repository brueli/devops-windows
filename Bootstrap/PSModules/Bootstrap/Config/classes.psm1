Import-Module $PSScriptRoot\..\..\..\..\PSModules\PSParameters\PSParameters.psm1


class ConfigSection {
    [BootstrapConfiguration] $Root

    ConfigSection([BootstrapConfiguration] $root, [Hashtable] $config, [string] $context) {
        $this.Root = $root
    }

    [PSObject] Require([PSObject] $value, [string] $hint) {
        return Assert-NotNull $value -Hint $hint
    }

    [PSObject] Require([PSObject] $value, [PSObject] $defaultValue, [string] $hint) {
        return Select-Value $value $defaultValue | Assert-NotNull -Hint $hint
    }

    [PSObject] SelectValue([PSObject] $value, [PSObject] $defaultValue) {
        return Select-Value $value $defaultValue
    }

    [PSObject] Eval([string] $value) {
        if ($value.StartsWith('[') -and $value.EndsWith(']')) {
            if ($value.StartsWith('[[')) {
                return $value.Substring(1)
            } else {
                $inlineScript = $value.Substring(1, $value.Length - 2)
                try {
                    return Invoke-Expression -Command $inlineScript
                } catch {
                    Write-Warning -Message "Inline script failed: $inlineScript. Did you forget to escape with '[['?"
                    return $value.Substring(1)
                }
            }
        } else {
            return $value
        }
    }
}


class BootstrapConfiguration : ConfigSection {

    static [Hashtable] $Registrations

    [Hashtable] $Config
    [System.Collections.Specialized.OrderedDictionary] $Sections

    static BootstrapConfiguration() {
        [BootstrapConfiguration]::Registrations = @{}
    }

    BootstrapConfiguration([Hashtable] $config) 
        : base($null, $config, '') 
    {
        $this.Sections = @{}
        $this.Config = $config

        foreach ($section_name in $config.Keys) {
            $registration = [BootstrapConfiguration]::Registrations[$section_name]
            if ($registration) {
                Write-Verbose "Begin parsing: $section_name"
                try {
                    $newSection = $registration.Factory.Build($this, $config[$section_name], $section_name)
                    $this.Sections[$section_name] = $newSection
                } catch {
                    Write-Error $_
                    Write-Error -Message "Could not parse section $section_name"
                } finally {
                    Write-Verbose "End parsing: $section_name"
                }
            }
        }
    }

    [void] Apply() {
        foreach ($section_name in $this.Sections.Keys) {
            $registration = [BootstrapConfiguration]::Registrations[$section_name]
            if ($registration) {
                try {
                    Write-Verbose "Begin processing: $section_name"
                    $registration.Processor.Invoke($this)
                } catch {
                    Write-Error $_
                    Write-Error -Message "section $section_name ended with errors"
                } finally {
                    Write-Verbose "End processing: $section_name"
                }
            }
        }
    }

    [string] ResolveThumbprint([string] $certificateId) {
        $certFile = $this.Sections.Certificates.Files | Where-Object { $_.Id -ieq $certificateId } | Select-Object -First 1
        if (!$certFile) {
            throw "Certificate.Id not found: $certificateId"
        }
        return $certFile.Thumbprint
    }
}


class ConfigFactory {
    ConfigFactory() {
        
    }

    [ConfigSection] Build([BootstrapConfiguration] $root, [Hashtable] $config, [string] $context) {
        throw "Not implemented"
    }
}


class ConfigProcessor {
    ConfigProcessor() {
        
    }

    [void] Invoke([BootstrapConfiguration] $configuration) {
        throw "Not implemented"
    }
}
