using module .\classes.psm1
using namespace .\classes.psm1


function New-BootstrapConfiguration {
    [OutputType([BootstrapConfiguration])]
    param(
        [Hashtable] $Config
    )
    return [BootstrapConfiguration]::new($Config)
}


function Register-BootstrapSection {
    param(
        [string] $Name,
        [ScriptBlock] $Factory,
        [ScriptBlock] $Processor
    )
    [BootstrapConfiguration]::Registrations[$Name] = @{ 
        Factory = $Factory.InvokeReturnAsIs()
        Processor = $Processor.InvokeReturnAsIs()
    }
}


function Get-BootstrapSection {
    param()
    return [BootstrapConfiguration]::Registrations
}


function Install-BootstrapConfiguration {
    param(
        [Parameter(Mandatory=$true)]
        [ValidateNotNull()]
        [BootstrapConfiguration] $Config
    )
    $Config.Apply()
}


Export-ModuleMember -Function New-BootstrapConfiguration, Register-BootstrapSection, Get-BootstrapSection, `
    Install-BootstrapConfiguration
