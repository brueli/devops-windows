using module ..\Config\classes.psm1
using namespace ..\Config\classes.psm1

Import-Module $PSScriptRoot\..\Config\Config.psm1
Import-Module $PSScriptRoot\functions.psm1


class WinRmListener : ConfigSection {
    [string] $Enabled
    [string] $Transport
    [int] $Port
    [string] $Certificate

    WinRmListener([BootstrapConfiguration] $root, [Hashtable] $config, [string] $context)
        : base($root, $config, $context)
    {
        $this.Enabled = $this.Require($config.Enabled, 'true', "$context.Enabled")
        $this.Transport = $this.Require($config.Transport, 'HTTPS', "$context.Transport")
        $this.Port = [int] $this.Require($config.Port, 5986, "$context.Port")
        $this.Certificate = $this.Require($config.Certificate, "$context.Certificate")
    }
}


class WinRmConfig : ConfigSection {
    [bool] $Present
    [bool] $Enabled
    [System.Collections.Generic.List[WinRmListener]] $Listeners

    WinRmConfig([BootstrapConfiguration] $root, [Hashtable] $config, [string] $context)
        : base($root, $config, $context)
    {
        $this.Present = $this.Require($config.Present, $true, "$context.Present")
        $this.Enabled = $this.Require($config.Enabled, $true, "$context.Enabled")

        $this.Listeners = [System.Collections.Generic.List[WinRmListener]]::new()
        for ($iListener = 0; $iListener -lt $config.Listeners.Count; $iListener++) {
            $listener = [WinRmListener]::new($root, $config.Listeners[$iListener], "$context.Listener[$iListener]")
            $this.Listeners.Add($listener)
        }
    }
}


class WinRmConfigFactory : ConfigFactory {
    WinRmConfigFactory() : base() {}
    
    [ConfigSection] Build([BootstrapConfiguration] $root, [Hashtable] $config, [string] $context) {
        return [WinRmConfig]::new($root, $config, $context)
    }
}


class WinRmConfigProcessor : ConfigProcessor {
    WinRmConfigProcessor() : base() {}

    [void] Invoke([BootstrapConfiguration] $configuration) {
        $winRmSection = [WinRmConfig] $configuration.Sections.WinRm
        if (!$winRmSection) {
            return
        }
        if (!$winRmSection.Present) {
            return
        }
        foreach ($listener in $winRmSection.Listeners) {
            $thumbprint = $configuration.ResolveThumbprint($listener.Certificate)
            Install-WinRmListener -Port $listener.Port -Transport $listener.Transport -Thumbprint $thumbprint -Enabled $listener.Enabled
        }
    }
}


Register-BootstrapSection -Name 'WinRm' -Factory {
    return [WinRmConfigFactory]::new()
} -Processor {
    return [WinRmConfigProcessor]::new()
}