
function Get-WinRmListeners {
    [CmdletBinding()]
    param()

    $result = @()
    $lines = winrm enumerate winrm/config/listener
    for ($iLine = 0; $iLine -lt $lines.Count; $iLine++) {
        if ($lines[$iLine] -ilike 'Listener') {
            $iLine++
            $listenerProps = @{}
            while ($lines[$iLine].StartsWith(' ')) {
                $listenerPropMatch = [Regex]::Match($lines[$iLine], '^\s+(\w+) = (.*)$')
                if ($listenerPropMatch.Success) {
                    $propName = $listenerPropMatch.Groups[1].Value
                    $propValue = $listenerPropMatch.Groups[2].Value
                    $listenerProps[$propName] = $propValue
                }
                $iLine++
            }
            $result += @( New-Object PSObject -Property $listenerProps )
        }
    }
    return $result
}

function Install-WinRmListener {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$false)]
        [string] $Port = 5986,

        [Parameter(Mandatory=$false)]
        [ValidateSet('HTTP','HTTPS')]
        [string] $Transport = 'HTTPS',

        [Parameter(Mandatory=$false)]
        [string] $Thumbprint = $null,

        [Parameter(Mandatory=$false)]
        [ValidateSet('true','false')]
        [string] $Enabled = 'true'
    )

    if ($Transport -ieq 'https') {
        if ([string]::IsNullOrWhiteSpace($Thumbprint)) {
            throw "Thumbprint is required"
        }
    }

    $allListeners = Get-WinRmListeners

    $existingListener = $allListeners | Where-Object { 
        ($_.Port -eq $Port) -and ($_.Transport -eq $Transport) 
    } | Select-Object -First 1

    if ($existingListener) {
        winrm set winrm/config/Listener?Address=*+Transport=$Transport "@{CertificateThumbprint=`"$Thumbprint`";Enabled=`"$Enabled`";Hostname=`"`"}"
    } else {
        winrm add winrm/config/Listener?Address=*+Transport=$Transport "@{CertificateThumbprint=`"$Thumbprint`";Port=`"$Port`";Hostname=`"`";Enabled=`"$Enabled`"}"
    }

}


Export-ModuleMember -Function Install-WinRmListener