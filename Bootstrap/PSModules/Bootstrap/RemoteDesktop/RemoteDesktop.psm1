using module ..\Config\classes.psm1
using namespace ..\Config\classes.psm1

Import-Module $PSScriptRoot\..\Config\Config.psm1


class RemoteDesktopConfig : ConfigSection {
    [bool] $Present
    [bool] $Enabled

    RemoteDesktopConfig([BootstrapConfiguration] $root, [Hashtable] $config, [string] $context)
        : base($root, $config, $context)
    {
        $this.Present = $this.Require($config.Present, "$context.Present")
        $this.Enabled = $this.Require($config.Enabled, "$context.Enabled")
    }
}


class RemoteDesktopConfigFactory : ConfigFactory {
    RemoteDesktopConfigFactory() : base() {}

    [ConfigSection] Build([BootstrapConfiguration] $root, [Hashtable] $config, [string] $context) {
        return [RemoteDesktopConfig]::new($root, $config, $context)
    }
}


class RemoteDesktopConfigProcessor : ConfigProcessor {
    RemoteDesktopConfigProcessor() : base() {}

    [void] Invoke([BootstrapConfiguration] $configuration) {
        $remoteDesktopConfig = [RemoteDesktopConfig] $configuration.Sections['RemoteDesktop']
        if ($remoteDesktopConfig.Present) {
            if ($remoteDesktopConfig.Enabled) {
                Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server' -name "fDenyTSConnections" -value 0
                Enable-NetFirewallRule -DisplayGroup "Remote Desktop"
            }
            else {
                Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server' -name "fDenyTSConnections" -value 1
                Disable-NetFirewallRule -DisplayGroup "Remote Desktop"
            }
        }
    }
}


Register-BootstrapSection -Name 'RemoteDesktop' -Factory {
    return [RemoteDesktopConfigFactory]::new()
} -Processor {
    return [RemoteDesktopConfigProcessor]::new()
}
