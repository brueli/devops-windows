[CmdletBinding()]
param(
    [string] $Config = '.\Bootstrap.config.psd1'
)

Import-Module $PSScriptRoot\..\PSModules\Config\Config.psm1 -Verbose -Force
Import-Module $PSScriptRoot\..\PSModules\Tls\Tls.psm1 -Verbose -Force
Import-Module $PSScriptRoot\..\PSModules\RemoteDesktop\RemoteDesktop.psm1 -Verbose -Force
Import-Module $PSScriptRoot\..\PSModules\Certificates\Certificates.psm1 -Verbose -Force
Import-Module $PSScriptRoot\..\PSModules\WinRm\WinRm.psm1 -Verbose -Force

$configPathResolved = (Resolve-Path $Config).Path

$bootstrapConfigRaw = Import-PowerShellDataFile -Path $configPathResolved

$bootstrapConfig = New-BootstrapConfiguration -Config $bootstrapConfigRaw

Install-BootstrapConfig -Config $bootstrapConfig
