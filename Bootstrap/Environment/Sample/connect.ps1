﻿<##
    Auto-generated shim for '../../../bin/connect.ps1'
#>
[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [Hashtable] $WinRmDefaults = @{
        ComputerName = 'localhost'
        Port = 5986
        UseSSL = $true
        Credential = @{
            UserName = 'vagrant'
            PasswordText = 'vagrant'
            PasswordHint = 'Enter password for user "vagrant"'
        }
        SessionOption = @{
            SkipCACheck = $true
            SkipCNCheck = $true
            SkipRevocationCheck = $true
        }
    },
    [Hashtable] $WinRmHosts = @{
        'mycomputer' = @{ 
            Port=10002 
        }
    }
)
$invocationArgs = @{
    'WinRmDefaults' = $WinRmDefaults
    'WinRmHosts' = $WinRmHosts
}
& "../../../bin/connect.ps1" @invocationArgs

