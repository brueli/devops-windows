﻿<##
    Auto-generated shim for '../../../bin/pack.ps1'
#>
[CmdletBinding()]
param(
    [string] $WorkingFolder = '../../..',
    [string[]] $Path = @(
        # PSModules + bin Files
        'PSModules'
        'Bootstrap/bin'
        'Bootstrap/PSModules'
        'Certificates/bin'
        'Certificates/PSModules'
        # Environments
        'Bootstrap/Environment/Sample'
        'Certificates/Environment/Sample'
    ),
    [string] $Destination = '.\setup.zip'
)
$invocationArgs = @{
    'WorkingFolder' = $WorkingFolder
    'Path' = $Path
    'Destination' = $Destination
}
& "../../../bin/pack.ps1" @invocationArgs

