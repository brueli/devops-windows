﻿<##
    Auto-generated shim for '..\..\bin\Start-Bootstrap.ps1'
#>
[CmdletBinding()]
param(
    [string] $Config = '.\Bootstrap.config.psd1'
)
$invocationArgs = @{
    'Config' = $Config
}
& "..\..\bin\Start-Bootstrap.ps1" @invocationArgs

