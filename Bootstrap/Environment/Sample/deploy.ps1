﻿<##
    Auto-generated shim for '../../../bin/deploy.ps1'
#>
[CmdletBinding()]
param(
    [string] $SetupZip = '.\setup.zip',
    [string] $TargetPath = 'C:\bruelisoft\remote-setup'
)
$invocationArgs = @{
    'SetupZip' = $SetupZip
    'TargetPath' = $TargetPath
}
& "../../../bin/deploy.ps1" @invocationArgs
