{
    Certificates = @{
        #DefaultPasswordText = 'mysecret'
        DefaultPasswordText = '[$env:PFX_PASSWORD]'
        #DefaultPasswordHint = 'Use "vagrant" for this demo'
        DefaultStore = 'Cert:\LocalMachine\My'

        Files = @(
            @{ 
                Id = 'Root'
                Type = 'CER' 
                Thumbprint = 'EB089ADBEF6E3B53B58F340A8B4CA9B22784E07F'
                FilePath = "$PSScriptRoot\..\..\..\Certificates\Environment\Sample\CER\RootCA.cer"
                #FileContent = ''
                TargetStore = 'Cert:\LocalMachine\Root' 
                #PasswordHint = ''
                #PasswordText = ''
            }

            @{
                Id = 'WinRm'
                Type = 'PFX'
                Thumbprint = '632BBB69CDBE718BDCEC92DCC6D844C0B9C888E9'
                FilePath = "$PSScriptRoot\..\..\..\Certificates\Environment\Sample\PFX\WinRm.pfx"
            }

            @{
                Id = 'Dsc'
                Type = 'PFX'
                Thumbprint = '6B6704B0367A22654B7CD444C0A52D5DD6B75E55'
                FilePath = "$PSScriptRoot\..\..\..\Certificates\Environment\Sample\PFX\Dsc.pfx"
            }
        )
    }

    Tls = @{
        Present = $true
        Enabled = $true
    }

    RemoteDesktop = @{
        Present = $true
        Enabled = $true
    }

    WinRm = @{
        Present = $true
        Enabled = $true

        Listeners = @(
            @{
                Enabled = 'true'
                Transport = 'HTTPS'
                Port = 5986
                Certificate = 'WinRm'
            }
        )
    }
}