﻿[CmdletBinding()]
param(
    [string] $ZoneName = 'vagrant.local',

    [Hashtable[]] $ARecords = @(
        @{ Name = 'devops'; IPv4Address = '192.168.50.3' }
    )
)

# install DNS Server feature
$dnsServerFeature = Get-WindowsFeature -Name DNS
if (!$dnsServerFeature.Installed) {
    Install-WindowsFeature -Name DNS -IncludeManagementTools
}

# forward lookup zone
# this server maintains the zone
# zone name: vagrant.local
# Create new zone file: vagrant.local.dns
# (zone file location: %SYSTEMROOT%\system32\dns)
# Do not allow dynamic updates
# Forward Queries to: 8.8.8.8


# create forward-lookup zone
# --------------------------
$existingZone = Get-DnsServerZone -Name $ZoneName -ErrorAction SilentlyContinue
if (!$existingZone) {
    $PSCmdlet.WriteObject("Creating DNS Forward-Lookup Zone: $ZoneName")
    Add-DnsServerPrimaryZone -Name $ZoneName -ZoneFile "${ZoneName}.dns" -DynamicUpdate None
} else {
    $PSCmdlet.WriteObject("DNS Forward-Lookup Zone exists: $ZoneName")
}


# add DNS forwarder
# -----------------
$existingForwarder = Get-DnsServerForwarder | Where-Object { $_.IPAddress -eq "8.8.8.8" } | Select-Object -First 1
if (!$existingForwarder) {
    $PSCmdlet.WriteObject("Adding DNS Forwarder: 8.8.8.8")
    Add-DnsServerForwarder -IPAddress 8.8.8.8
} else {
    $PSCmdlet.WriteObject("DNS Forwarder exists: 8.8.8.8")
}


# update DNS records
# ------------------
for ($iRecord = 0; $iRecord -lt $ARecords.Count; $iRecord++) {
    $current_record = $ARecords[$iRecord]
    $existingRecord = Get-DnsServerResourceRecord -ZoneName $ZoneName -Name $current_record.Name -ErrorAction SilentlyContinue
    if ($existingRecord) {
        if ($existingRecord.RecordData.IPv4Address -ne $current_record.IPv4Address) {
            $PSCmdlet.WriteObject("Updating A-Record: $($current_record.Name) -> $($current_record.IPv4Address)")
            $updatedRecord = $existingRecord.Clone()
            $updatedRecord.RecordData.IPv4Address = $current_record.IPv4Address
            Set-DnsServerResourceRecord -ZoneName $ZoneName -OldInputObject $existingRecord -NewInputObject $updatedRecord
        } else {
            $PSCmdlet.WriteObject("Found A-Record: $($current_record.Name) -> $($current_record.IPv4Address)")
        }
    } else {
        $PSCmdlet.WriteObject("Adding A-Record: $($current_record.Name) -> $($current_record.IPv4Address)")
        $newRecord = Add-DnsServerResourceRecordA -ZoneName $ZoneName -Name $current_record.Name -IPv4Address $current_record.Address
    }
}