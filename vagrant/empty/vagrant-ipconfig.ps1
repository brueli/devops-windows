[CmdletBinding()]
param(
    [string] $IpAddress = '192.168.50.3',
    [string] $Netmask = '255.255.255.0',
    [string] $DefaultGateway = '192.168.50.1',
    [string] $PrimaryDns = '192.168.50.3',
    [string] $SecondaryDns = $null,
    [string] $FirewallZone = 'Private',
    [string] $FirewallState = 'off'
)


function Find-Adapter {
    $lines = netsh interface ipv4 show addresses
    for ($iLine = 0; $iLine -lt $lines.Count; $iLine++) {
        $configMatch = [Regex]::Match($lines[$iLine], 'Configuration for interface "(.*?)"')
        if ($configMatch.Success) {
            $iLine++
            while ($lines[$iLine].StartsWith(" ")) {
                $ipAddressMatch = [Regex]::Match($lines[$iLine], "\s+IP Address:\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})")
                if ($ipAddressMatch.Success) {
                    $ip_address = [System.Net.IPAddress]::Parse($ipAddressMatch.Groups[1].Value)
                    if ($ip_address.ToString() -eq $IpAddress) {
                        return $configMatch.Groups[1].Value
                    }
                }
                $iLine++
            }
        }
    }
    throw "Adapter not found"
}


$adapterName = Find-Adapter

# set default gateway
netsh interface ipv4 add route 0.0.0.0/0 $adapterName $DefaultGateway

# set primary DNS
if (![string]::IsNullOrWhiteSpace($PrimaryDns)) {
    netsh interface ip set dns name="$adapterName" static $PrimaryDns
}

# set seconary DNS
if (![string]::IsNullOrWhiteSpace($SecondaryDns)) {
    netsh interface ip add dns name="$adapterName" $SecondaryDns index=2
}

# assign adapter to firewall zone
while ( (Get-NetConnectionProfile -InterfaceAlias $adapterName).Name -eq "Identifying...") {
    Start-Sleep -Milliseconds 500
}
Set-NetConnectionProfile -InterfaceAlias $adapterName -NetworkCategory $FirewallZone

# enable/disable firewall for zone
$zone_name = "{0}profile" -f $FirewallZone.ToLower()
netsh advfirewall set $zone_name state $FirewallState
