DevOps Development Environment
==============================

Version 0.2.0

> **This is a pre-release.**
>
> It may contain bugs resulting in unintended behaviour and or system failure<br />
> Use with caution.


Table Of Contents
-----------------

* [Project Setup](docs/Setup.md)
* [Shim Generator](docs/Shim-Generator.md)
* [Connect, Pack and Deploy Scripts](docs/Connect-Pack-And-Deploy.md)
* [Certificate Creation Toolkit](docs/Certificates.md)
* [Bootstrap](docs/Bootstrap.md)
