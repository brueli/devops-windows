
function Assert-NotNull {
    param(
        [Parameter(Mandatory=$false,ValueFromPipeline=$true)]
        [PSObject] $Value,
        [Parameter(Mandatory=$false)]
        [string] $Hint
    )
    if ($null -eq $Value) {
        $errorMsg = if ($Hint) { "Value is required: $Hint" } else { "Value is required" }
        throw $errorMsg
    }
    return $Value
}


function Select-Value {
    param(
        [Parameter(Mandatory=$false,Position=0)]
        [PSObject] $Value,
        [Parameter(Mandatory=$false,Position=1)]
        [PSObject] $Default
    )
    if ($null -eq $Value) {
        return $Default
    }
    if ($Value -is [bool]) {
        return $Value
    }
    if ($Value) {
        return $Value
    }
    return $Default
}


Export-ModuleMember -Function Assert-NotNull, Select-Value