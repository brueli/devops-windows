# ./PSParameters.Tests.ps1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = 'PSParameters.psm1'
Import-Module $here\$sut -Verbose -Force

Describe("PSParameters/Assert-NotNull") {
    it("Throws an exception if value is `$null") {
        { Assert-NotNull $null 'Choice' } | Should Throw 'Value is required: Choice'
    }
    it("Returns `$Value if value is given") {
        Assert-NotNull $false 'Choice' | Should Be $false
        Assert-NotNull $true 'Choice' | Should Be $true
    }
}

Describe("PSParameters/Select-Value") {
    it("Returns default value if value is `$null") {
        Select-Value $null $false | Should Be $false
        Select-Value $null $true | Should Be $true
    }
    it("Returns `$Value if value is given") { 
        Select-Value $false $true | Should Be $false
        Select-Value $true $true | Should Be $true
    }
}

Describe("PSParameters/IntegrationTests") {
    it("Select-Value and Assert-NotNull are chainable") {
        { Select-Value $null $null | Assert-NotNull -Hint 'Choice' } | Should Throw 'Value is required: Choice'
    }
}
