
function New-PSShimFile {
    [CmdletBinding(SupportsShouldProcess=$true)]
    param(
        [string] $Command,
        [string] $Destination,
        [switch] $Force
    )

    # test if command exists
    $target_command = Get-Command $Command -ErrorAction SilentlyContinue
    if (!$target_command) {
        throw "Command not found: $Command"
    }

    # test output path
    if (!(Test-Path -Path $Destination -PathType Leaf -IsValid)) {
        throw "Destination is not valid: $Destination"
    }

    # abort if output file already exists
    if (!$Force.IsPresent) {
        if (Test-Path -Path $Destination -PathType Leaf) {
            throw "Destination file already exists: $Destination"
        }
    }

    # test if target command is a script
    if (!$target_command.ScriptBlock) {
        throw "Command must have ScriptBlock"
    }

    # test if AST is available
    if (!$target_command.ScriptBlock.ast) {
        throw "Command must have a valid AST"
    }

    # find param() block, parameters and [CmdletBinding()] if any
    $main_ast = $target_command.ScriptBlock.ast.FindAll({ param($ast)
        $ast.ParamBlock
    }, $true) | Select-Object -First 1

    $cmdletBinding_attribute = $main_ast.ParamBlock.Attributes.Where({ $_.TypeName.FullName -eq 'CmdletBinding' }) | Select-Object -First 1
    
    # generate the shim
    $shim_script = [System.Text.StringBuilder]::new()

    # emit comment
    $shim_script.AppendLine(@"
<##
    Auto-generated shim for '$Command'
#>
"@) | Out-Null

    # emit [CmdletBinding] if required
    if ($cmdletBinding_attribute) {
        $shim_script.AppendLine( $cmdletBinding_attribute.ToString() ) | Out-Null
    }

    # emit param() block
    $shim_script.AppendLine( $main_ast.ParamBlock.ToString() ) | Out-Null

    # emit hashtable to splat arguments
    $shim_script.AppendLine( "`$invocationArgs = @{")
    foreach ($p in $main_ast.ParamBlock.Parameters) {
        $shim_script.AppendLine("    '$($p.Name.VariablePath.UserPath)' = $($p.Name)") | Out-Null
    }
    $shim_script.AppendLine("}") | Out-Null

    # emit command invocation
    $shim_script.AppendLine("& `"$Command`" @invocationArgs") | Out-Null

    if ($PSCmdlet.ShouldProcess($Destination, 'Create shim script')) {
        $shim_script.ToString() | Out-File -FilePath $Destination -Encoding utf8
    } else {
        $shim_script.ToString() | Out-Host
    }
}


Export-ModuleMember -Function New-PSShimFile