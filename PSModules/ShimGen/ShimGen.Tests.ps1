# ./ShimGen.Tests.ps1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = 'ShimGen.psm1'
Import-Module $here\$sut -Verbose -Force

Describe "ShimGen/New-PSShimFile" {
    It "Creates a valid shim" {
        
        New-PSShimFile `
            -Command $here\..\..\Bootstrap\bin\Start-Bootstrap.ps1 `
            -Destination $here\Start-Bootstrap.ps1 `
            -Force

        $orig_script = Get-Command $here\..\..\Bootstrap\bin\Start-Bootstrap.ps1
        $shim_script = Get-Command $here\Start-Bootstrap.ps1

        foreach ($p in $orig_script.ScriptBlock.Ast.ParamBlock.Parameters) {
            $shim_script.ResolveParameter( $p.Name.VariablePath.UserPath )
        }

        Remove-Item $here\Start-Bootstrap.ps1

    }
}