# ./PSConfigFile.Tests.ps1
using module ./PSConfigFile.psm1
using namespace ./PSConfigFile.psm1

<#
    configuration classes for testing 
#>
class SampleConfiguration : BaseConfiguration {
    [bool] $boolValue
    [int] $intValue
    [string] $stringValue
    [SampleModule] $sampleModule

    SampleConfiguration([Hashtable] $config, [ConfigurationFactory] $factory) 
        : base($config, $factory)
    {
        $this.boolValue = $this.Require($config.boolValue, "boolValue")
        $this.intValue = $this.SelectValue($config.intValue, 80)
        $this.stringValue = $this.Require($config.stringValue, "stringValue")
    }
}

class SampleConfigurationBuilder : ConfigBuilder {
    SampleConfigurationBuilder() {}

    [SampleConfiguration] Build([Hashtable] $config, [ConfigurationFactory] $factory) {
        return [SampleConfiguration]::new($config, $factory) 
    }
}

class SampleModule : ConfigSection {
    [bool] $boolValue
    [int] $intValue
    [string] $stringValue

    SampleModule([BaseConfiguration] $root, [Hashtable] $config, [string] $context)
        : base($root, $config, $context)
    {
        $this.boolValue = $this.Require($config.boolValue, "boolValue")
        $this.intValue = $this.SelectValue($config.intValue, 80)
        $this.stringValue = $this.Require($config.stringValue, "stringValue")
    }
}

class SampleModuleBuilder : ModuleBuilder {
    SampleModuleBuilder() {}

    [SampleModule] Build([BaseConfiguration] $root, [Hashtable] $config, [string] $section) {
        return [SampleModule]::new($root, $config, $section)
    }
}


Describe("PSConfigFile") {
    it("Can build a configuration object from config data") {
        $configData = @{
            boolValue = $true
            intValue = 80
            stringValue = 'Hello World'
        }
        
        $factory = [ConfigurationFactory]::new({
            [SampleConfigurationBuilder]::new() 
        })

        $result = [SampleConfiguration] $factory.Build($configData)

        $result.boolValue | Should Be $configData.boolValue
        $result.intValue | Should Be $configData.intValue
        $result.stringValue | Should Be $configData.stringValue
    }

    it("Can build a configuration object with sections from config data") {
        $configData = @{
            boolValue = $true
            intValue = 80
            stringValue = 'Hello World'
            sampleModule = @{
                boolValue = $true
                intValue = 80
                stringValue = 'Hello World'
            }
        }
        
        $factory = [ConfigurationFactory]::new({
            [SampleConfigurationBuilder]::new() 
        })
        $factory.Register('sampleModule', { [SampleModuleBuilder]::new() })

        $result = [SampleConfiguration] $factory.Build($configData)

        $result.boolValue | Should Be $configData.boolValue
        $result.intValue | Should Be $configData.intValue
        $result.stringValue | Should Be $configData.stringValue

        $result.sampleModule.boolValue | Should Be $configData.sampleModule.boolValue
        $result.sampleModule.intValue | Should Be $configData.sampleModule.intValue
        $result.sampleModule.stringValue | Should Be $configData.sampleModule.stringValue
    }
}