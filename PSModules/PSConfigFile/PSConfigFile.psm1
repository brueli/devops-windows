Import-Module $PSScriptRoot\..\PSParameters\PSParameters.psm1

<#
    implements a config section
#>
class ConfigSection {
    [BaseConfiguration] $Root

    ConfigSection([BaseConfiguration] $root, [Hashtable] $config, [string] $context) {
        $this.Root = $root
    }

    [PSObject] Require([PSObject] $value, [string] $hint) {
        return Assert-NotNull $value -Hint $hint
    }

    [PSObject] Require([PSObject] $value, [PSObject] $defaultValue, [string] $hint) {
        return Select-Value $value $defaultValue | Assert-NotNull -Hint $hint
    }

    [PSObject] SelectValue([PSObject] $value, [PSObject] $defaultValue) {
        return Select-Value $value $defaultValue
    }
}

<#
    implements a extendable configuration object
#>
class BaseConfiguration : ConfigSection {

    [Hashtable] $Config

    BaseConfiguration([Hashtable] $config, [ConfigurationFactory] $factory) 
        : base($null, $config, '') 
    {
        # init members
        $this.Config = $config

        # build sections from registered modules
        foreach ($section_name in $config.Keys) {
            $module = [ModuleBuilder] $factory.Modules[$section_name]
            if ($module) {
                Write-Verbose "Found module: $section_name"
                try {
                    $newSection = $module.Build($this, $config[$section_name], $section_name)
                    $targetProp = $this.PSObject.Properties[$section_name]
                    if ($targetProp) {
                        # update declare module member
                        $targetProp.Value = $newSection
                    } else {
                        # add dynamic module member
                        $autoProperty = [System.Management.Automation.PSNoteProperty]::new($section_name, $newSection)
                        $this.PSObject.Members.Add($autoProperty)
                    }
                } catch {
                    Write-Error $_
                    Write-Error -Message "Failed to parse module: $section_name"
                } finally {
                    Write-Verbose "End module: $section_name"
                }
            }
        }
    }
}


class ModuleBuilder {
    [ConfigSection] Build([BaseConfiguration] $root, [Hashtable] $config, [string] $context) {
        throw "Not implemented"
    }
}


class ConfigBuilder {
    [BaseConfiguration] Build([Hashtable] $config, [ConfigurationFactory] $factory) {
        throw "Not implemented"
    }
}


class ConfigurationFactory {

    [Hashtable] $Modules
    [ConfigBuilder] $Builder

    ConfigurationFactory([ScriptBlock] $configFactoryMethod) {
        $this.Modules = @{}
        $this.Builder = [ConfigBuilder] $configFactoryMethod.InvokeReturnAsIs()
    }

    [void] Register([string] $moduleName, [ScriptBlock] $moduleFactoryMethod) {
        $this.Modules[$moduleName] = [ModuleBuilder] $moduleFactoryMethod.InvokeReturnAsIs()
    }

    [BaseConfiguration] Build([Hashtable] $config) {
        return $this.Builder.Build($config, $this)
    }
}