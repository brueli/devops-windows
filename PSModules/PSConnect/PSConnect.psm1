
class WinRmHost {
    [string] $ComputerName
    [int] $Port
    [bool] $UseSSL
    [PSCredential] $Credential
    [System.Management.Automation.Remoting.PSSessionOption] $SessionOption

    <##
    # Construct a new WinRmHost object
    #>
    WinRmHost([string] $Context, [Hashtable] $HostConfig, [Hashtable] $DefaultConfig) {
        $this.ComputerName = Select-Value $HostConfig.ComputerName $DefaultConfig.ComputerName `
            | Assert-NotNull -Hint "$Context.ComputerName"

        $this.Port = Select-Value $HostConfig.Port $DefaultConfig.Port `
            | Assert-NotNull -Hint "$Context.Port"

        $this.UseSSL = Select-Value $HostConfig.UseSSL $DefaultConfig.UseSSL

        $thisCredentialOpts = Select-Value $HostConfig.Credential @{}
        $defaultCredentialOpts = Select-Value $DefaultConfig.Credential @{}

        $username = Select-Value $thisCredentialOpts.UserName $defaultCredentialOpts.UserName `
            | Assert-NotNull -Hint "$Context.UserName"
        $passwordText = Select-Value $thisCredentialOpts.PasswordText $defaultCredentialOpts.PasswordText
        if ([string]::IsNullOrEmpty($passwordText)) {
            $passwordHint = Select-Value $thisCredentialOpts.PasswordHint $defaultCredentialOpts.PasswordHint `
                | Assert-NotNull -Hint "$Context.PasswordHint"
            $_credential = Get-Credential -UserName $username -Message $passwordHint
        } else {
            $_credential = [PSCredential]::new(
                $username,
                (ConvertTo-SecureString $passwordText -AsPlaintext -Force)
            )
        }
        $this.Credential = Assert-NotNull $_credential -Hint "$Context.Credential"

        $thisSessionOpts = Select-Value $HostConfig.SessionOption @{}
        $defaultSessionOpts = Select-Value $DefaultConfig.SessionOption @{}
        $sessionOpts = @{
            SkipCNCheck = Select-Value $thisSessionOpts.SkipCNCheck $defaultSessionOpts.SessionOption.SkipCNCheck
            SkipCACheck = Select-Value $thisSessionOpts.SkipCACheck $defaultSessionOpts.SessionOption.SkipCACheck
            SkipRevocationCheck = Select-Value $thisSessionOpts.SkipRecovationCheck $defaultSessionOpts.SkipRevocationCheck
        }
        $this.SessionOption = New-PSSessionOption @sessionOpts
    }

    <##
    # Return a splattable arguments hashtable
    #>
    [Hashtable] ToArgs() {
        return @{
            ComputerName = $this.ComputerName
            Port = $this.Port
            UseSSL = $this.UseSSL
            Credential = $this.Credential
            SessionOption = $this.SessionOption
        }
    }
}


function Get-WinRmHosts {
    [CmdletBinding()]
    [OutputType([System.Collections.Specialized.OrderedDictionary])]
    param(
        [Hashtable] $WinRmHosts,
        [Hashtable] $WinRmDefaults
    )
    $result = [System.Collections.Specialized.OrderedDictionary]::new()
    foreach ($hostname in $WinRmHosts.Keys) {
        $hostentry = $WinRmHosts[$hostname]
        $result[$hostname] = [WinRmHost]::new("WinRmHosts[$hostname]", $hostentry, $WinRmDefaults)
    }
    return $result
}


Export-ModuleMember -Function Get-WinRmHosts
