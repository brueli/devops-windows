# ./PSConnect.Tests.ps1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = 'PSConnect.psm1'
Import-Module $here\$sut -Verbose -Force

$WinRmDefaults = @{
    ComputerName = 'localhost'
    Port = 5986
    UseSSL = $true
    Credential = @{
        UserName = 'vagrant'
        PasswordText = $env:USERPASSWORD
        PasswordHint = 'Enter password for user "vagrant"'
    }
    SessionOption = @{
        SkipCACheck = $true
        SkipCNCheck = $true
        SkipRevocationCheck = $true
    }
}

$WinRmHosts = @{
    'mycomputer' = @{ 
        Port = 10002
        Credential = @{ PasswordText = 'vagrant' } 
    }
}

$WinRmHosts2 = @{
    'mycomputer' = @{ 
        Port = 10002
        Credential = @{ PasswordText = 'vagrant' } 
    }
    'myothercomputer' = @{ 
        Port = 10005
        Credential = @{ PasswordHint = 'Use "mysecret"' } 
    }
}


Describe("PSConnect/Get-WinRmHosts") {
    it("Should return New-PSSession arguments for valid computernames") {
        $result = Get-WinRmHosts $WinRmHosts $WinRmDefaults
        $result['mycomputer'] | Should Not Be $null
    }
    
    it("Should not return New-PSSession arguments for non-existing computernames") {
        $result = Get-WinRmHosts $WinRmHosts $WinRmDefaults
        $result['hubbelflubb'] | Should Be $null
    }

    it("Should return complete winrm session arguments for a computer") {
        $result = Get-WinRmHosts $WinRmHosts2 $WinRmDefaults
        $result['mycomputer'] | Should Not Be $null
        $result['mycomputer'].Port | Should Be 10002
        $result['mycomputer'].ComputerName | Should Be 'localhost'
        $result['mycomputer'].UseSSL | Should Be $true
        $result['mycomputer'].Credential.UserName | Should Be 'vagrant'
        $result['mycomputer'].SessionOption.SkipCACheck | Should Be $true
        $result['mycomputer'].SessionOption.SkipCNCheck | Should Be $true
        $result['mycomputer'].SessionOption.SkipRevocationCheck | Should Be $true
    }
}
