
function ConvertTo-Plaintext {
    [OutputType([string])]
    param(
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [SecureString] $SecureString
    )
    $BSTR = [IntPtr]::Zero
    $result = [string] $null
    try {
        $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecureString)
        $result = [System.Runtime.InteropServices.Marshal]::PtrToStringBSTR($BSTR)
    }
    finally {
        if ($BSTR -ne [IntPtr]::Zero) { [System.Runtime.InteropServices.Marshal]::ZeroFreeBSTR($BSTR) }
        Remove-Variable -Name BSTR
    }
    return $result
}


class PSVaultKey : System.IDisposable {
    [byte[]] $Key
    [byte[]] $IV

    PSVaultKey([byte[]] $key, [byte[]] $iv) {
        $this.Key = $key
        $this.IV = $iv
    }

    [void] Dispose() {
        $this.Key = $null
        $this.IV = $null
    }

    static [PSVaultKey] DeriveKey([SecureString] $password, [byte[]] $saltBytes, [int] $rounds) {
        $hashProvider = [System.Security.Cryptography.SHA384Managed]::new()
        
        $passwordBytes = [PSVaultKey]::GetPasswordBytes($password)
        $keyBytes = [PSVaultKey]::Xor($passwordBytes, $saltBytes)
        
        for ($i = 0; $i -lt $rounds; $i++) {
            $keyBytes = $hashProvider.ComputeHash($keyBytes)
        }

        $newKey = $keyBytes[0..31]
        $newIv = $keyBytes[32..48]
        return [PSVaultKey]::new($newKey, $newIv)
    }

    hidden static [byte[]] GetPasswordBytes([SecureString] $password) {
        $passwordText = $password | ConvertTo-Plaintext
        return [System.Text.Encoding]::UTF8.GetBytes($passwordText)
    }

    hidden static [byte[]] Xor([byte[]] $a, [byte[]] $b) {
        if (!$a) {
            throw 'Array $a cannot be empty'
        }
        if (!$b) {
            throw 'Array $b cannot be empty'
        }
        $L = [Math]::Max($a.Length, $b.Length)
        $result = [byte[]]::new($L)
        for ($i = 0; $i -lt $L; $i++) {
            $ia = $i % $a.Length
            $ib = $i % $b.Length
            $result[$i] = $a[$ia] -bxor $b[$ib]
        }
        return $result
    }
}


class PSVaultFile {

    [string] $VaultFile
    [string] $PlaintextFile
    [int] $Rounds
    [string] $Salt
    [System.Security.Cryptography.RNGCryptoServiceProvider] $Rng

    [System.Collections.Specialized.OrderedDictionary] $Data
    [string] $Comment
    [int] $Version

    PSVaultFile() {
        $this.VaultFile = '.\vault.encrypted'
        $this.PlaintextFile = '.\vault.json'
        $this.Rounds = 50000
        $this.Rng = [System.Security.Cryptography.RNGCryptoServiceProvider]::new()
        $this.Salt = $null

        $this.Data = @{}
        $this.Comment = $null
        $this.Version = 3
    }

    hidden [byte[]] GetRandomSalt([int] $length) {
        [byte[]] $saltBytes = [byte[]]::new($length * 3 / 4)
        $this.Rng.GetBytes($saltBytes)
        return $saltBytes
    }

    hidden [string] ChangeFileExtension([string] $file, [string] $newExtension) {
        return -join ([System.IO.Path]::GetFileNameWithoutExtension($file), $newExtension)
    }

    hidden [string] EncryptData([string] $plaintext, [PSVaultKey] $vaultKey) {
        $resultBytes = [byte[]]::new(0)
        $aesProvider = [System.Security.Cryptography.AesManaged]::new()
        try {
            $aesProvider.Key = $vaultKey.Key
            $aesProvider.IV = $vaultKey.IV
            $aesProvider.Mode = [System.Security.Cryptography.CipherMode]::CBC
            $aesProvider.Padding = [System.Security.Cryptography.PaddingMode]::PKCS7
            $encryptor = $aesProvider.CreateEncryptor($aesProvider.Key, $aesProvider.IV)
            try {
                $msEncrypt = [System.IO.MemoryStream]::new()
                try {
                    $csEncrypt = [System.Security.Cryptography.CryptoStream]::new($msEncrypt, $encryptor, [System.Security.Cryptography.CryptoStreamMode]::Write)
                    try {
                        $swEncrypt = [System.IO.StreamWriter]::new($csEncrypt, [System.Text.Encoding]::UTF8)
                        try {
                            $swEncrypt.Write($plaintext)
                            $swEncrypt.Flush()
                        } finally {
                            $swEncrypt.Dispose()
                        }
                        $resultBytes = $msEncrypt.ToArray()
                    } finally {
                        $csEncrypt.Dispose()
                    }
                } finally {
                    $msEncrypt.Dispose()   
                }
            } finally {
                $encryptor.Dispose()
            }
        } finally {
            $aesProvider.Dispose()
        }
        return [Convert]::ToBase64String($resultBytes)
    }

    hidden [string] DecryptData([string] $ciphertext, [PSVaultKey] $vaultKey) {
        $decryptedMessage = $null
        $ciphertextBytes = [Convert]::FromBase64String($ciphertext)
        $aesProvider = [System.Security.Cryptography.AesManaged]::new()
        try {
            $aesProvider.Key = $vaultKey.Key
            $aesProvider.IV = $vaultKey.IV
            $aesProvider.Mode = [System.Security.Cryptography.CipherMode]::CBC
            $aesProvider.Padding = [System.Security.Cryptography.PaddingMode]::PKCS7
            $decryptor = $aesProvider.CreateDecryptor($aesProvider.Key, $aesProvider.IV)
            try {
                $msDecrypt = [System.IO.MemoryStream]::new($ciphertextBytes)
                try {
                    $csDecrypt = [System.Security.Cryptography.CryptoStream]::new($msDecrypt, $decryptor, [System.Security.Cryptography.CryptoStreamMode]::Read)
                    try {
                        $srDecrypt = [System.IO.StreamReader]::new($csDecrypt, [System.Text.Encoding]::UTF8)
                        try {
                            $decryptedMessage = $srDecrypt.ReadToEnd()
                        } finally {
                            $srDecrypt.Dispose()
                        }
                    } finally {
                        $csDecrypt.Dispose()
                    }
                } finally {
                    $msDecrypt.Dispose()
                }
            } finally {
                $decryptor.Dispose()
            }
        } finally {
            $aesProvider.Dispose()
        }
        return $decryptedMessage
    }

    [void] Load([string] $unencryptedFile) {
        $this.PlaintextFile = $unencryptedFile
        $this.VaultFile = $this.ChangeFileExtension($unencryptedFile, '.encrypted')

        $vaultData = Get-Content $this.PlaintextFile | ConvertFrom-Json
        $this.Version = [int]::Parse($vaultData.version)
        $this.Comment = $vaultData.comment

        $newData = [System.Collections.Specialized.OrderedDictionary]::new()
        foreach ($p in $vaultData.data.PSObject.Properties) {
            $newData[$p.Name] = $p.Value
        }
        $this.Data = $newData
    }

    [void] Load([string] $encryptedFile, [SecureString] $password) {

        $vaultData = Get-Content $encryptedFile | ConvertFrom-Json
        $this.Version = $vaultData.version
        $this.Comment = $vaultData.comment
        $this.Rounds = $vaultData.rounds
        $this.Salt = $vaultData.salt
        $encryptedData = $vaultData.data
        $saltBytes = [Convert]::FromBase64String($this.Salt)

        $vaultKey = [PSVaultKey]::DeriveKey($password, $saltBytes, $this.Rounds)
        $decryptedData = $this.DecryptData($encryptedData, $vaultKey)
        $vaultKey.Dispose()

        $dataObj = $decryptedData | ConvertFrom-Json
        foreach ($p in $dataObj.PSObject.Properties) {
            $this.Data[$p.Name] = $p.Value
        }
    }

    [void] Save([string] $encryptedFile, [SecureString] $password) {

        $saltBytes = $this.GetRandomSalt(64)
        $this.Salt = [Convert]::ToBase64String($saltBytes)
        $vaultKey = [PSVaultKey]::DeriveKey($password, $saltBytes, $this.Rounds)

        $dataString = $this.Data | ConvertTo-Json -Depth 10 -Compress
        $encryptedData = $this.EncryptData($dataString, $vaultKey)
        $vaultKey.Dispose()

        $vaultData = @{
            version = $this.Version
            comment = $this.Comment
            algorithm = 'aes:cbc:pkcs7'
            rounds = $this.Rounds
            salt = $this.Salt
            data = $encryptedData
        }

        $vaultData | ConvertTo-Json -Depth 10 | Out-File -FilePath $encryptedFile -Encoding utf8
    }

    [void] Save([string] $unencryptedFile) {
        
        $vaultData = @{
            version = $this.Version
            comment = $this.Comment
            data = $this.Data
        }

        $vaultData | ConvertTo-Json -Depth 10 | Out-File -FilePath $unencryptedFile -Encoding utf8
    }

}