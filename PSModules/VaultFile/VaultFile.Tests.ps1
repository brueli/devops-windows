# ./VaultFile.Tests.ps1
using module .\model.psm1
using namespace .\model.psm1

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = 'VaultFile.psm1'

Import-Module $here\$sut -Verbose -Force

Describe "VaultFile.psm1/New-Vault" {
    It "Returns a new PSVaultFile object" {
        $vault = New-Vault
        $vault -is [PSVaultFile] | Should Be $true
    }
}

Describe "model.psm1/PSVaultKey" {
    It "takes a 256-bit key and a 128-bit IV as constructor arguments" {
        $key = [byte[]]::new(32)
        $iv = [byte[]]::new(16)
        $vaultKey = [PSVaultKey]::new($key, $iv)
        $vaultKey.Key | Should BeExactly $key
        $vaultKey.IV | Should BeExactly $iv
    }

    It "256-bit key and 128-bit IV can be derived from password" {

        $password = ConvertTo-SecureString -String 'mysecret' -AsPlainText -Force

        $saltBytes = [byte[]]::new(64)
        $rng = [System.Security.Cryptography.RNGCryptoServiceProvider]::new()
        $rng.GetBytes($saltBytes)

        # use some rounds
        if ($PSDebugContext) {
            $rounds = 1
        } else {
            $rounds = 50000
        }
        
        $vaultKey = [PSVaultKey]::DeriveKey($password, $saltBytes, $rounds)
        Write-Host "Key =", ([Convert]::ToBase64String($vaultKey.Key))
        Write-Host "IV  =", ([Convert]::ToBase64String($vaultKey.IV))
    }

    It "Deriving a key from a password takes at least 500ms" {

        $password = ConvertTo-SecureString -String 'mysecret' -AsPlainText -Force

        $saltBytes = [byte[]]::new(64)
        $rng = [System.Security.Cryptography.RNGCryptoServiceProvider]::new()
        $rng.GetBytes($saltBytes)

        # increase rounds to achieve 500ms for DeriveKey() to pass the test
        # use some rounds
        if ($PSDebugContext) {
            $rounds = 1
        } else {
            $rounds = 300000
        }
        
        $stopwatch = [System.Diagnostics.Stopwatch]::StartNew()
        $vaultKey = [PSVaultKey]::DeriveKey($password, $saltBytes, $rounds)
        $stopwatch.Stop()
        $stopwatch.ElapsedMilliseconds | Should BeGreaterThan 500

        Write-Host "Key =", ([Convert]::ToBase64String($vaultKey.Key))
        Write-Host "IV  =", ([Convert]::ToBase64String($vaultKey.IV))
    }
}


Describe "model.psm1/PSVaultFile" {
    It "can load a plaintext vault file" {
        $vault = New-Vault
        $vault.Load("$PSScriptRoot\ref\vault.json")

        $vault.Version | Should Be 3
        $vault.Comment | Should Be 'comment field to describe contents of the vault or leave a password hint'
        $vault.Data.Length | Should Be 1
        $vault.Data['SecretValue'] | Should Be 'My Secret Value!'
    }

    It "can load a encrypted vault file" {
        $password = ConvertTo-SecureString -String 'mysecret' -AsPlainText -Force

        $vault = New-Vault
        $vault.Load("$PSScriptRoot\ref\vault.encrypted", $password)

        $vault.Comment | Should Be 'Test'
        $vault.Data['SecretValue'] | Should Be 'My secret value!'
        $vault.Data['OtherValue'] | Should Be 'My other secret!'
    }

    It "can save vault data into an encrypted file" {
        $password = ConvertTo-SecureString -String 'mysecret' -AsPlainText -Force

        $vault = New-Vault
        $vault.Load("$PSScriptRoot\ref\vault.json")

        $vault.Rounds = 1
        $vault.Comment = 'Test'

        $vault.Data['SecretValue'] = 'My secret value!'
        $vault.Data['OtherValue'] = 'My other secret!'

        $vault.Save("$PSScriptRoot\ref\vault1.encrypted", $password)
        Get-Content "$PSScriptRoot\ref\vault1.encrypted" | Out-Host

        $vault2 = New-Vault
        $vault2.Load("$PSScriptRoot\ref\vault1.encrypted", $password)

        foreach ($k in $vault2.Data.Keys) {
            $vault.Data[$k] | Should Not Be $null
            $vault2.Data[$k] | Should Be $vault.Data[$k]
        }

        Remove-Item "$PSScriptRoot\ref\vault1.encrypted"
    }

    It "can save vault data into a unencrypted file" {
        $password = ConvertTo-SecureString -String 'mysecret' -AsPlainText -Force

        $vault = New-Vault
        $vault.Load("$PSScriptRoot\ref\vault.encrypted", $password)

        $vault.Save("$PSScriptRoot\ref\vault2.json")

        Get-Content "$PSScriptRoot\ref\vault2.json" | Out-Host
    }
}