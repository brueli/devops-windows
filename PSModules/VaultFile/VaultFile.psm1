using module .\model.psm1
using namespace .\model.psm1

function New-Vault {
    [OutputType([PSVaultFile])]
    param()
    return [PSVaultFile]::new()
}
